(function($, tableau) {
  /**
   * Our main Web Data Connector object.
   *
   * Needs at least the following functions:
   * init, shutdown, getColumnHeaders and getTableData
   */
  var connector = tableau.makeConnector();

  /**
   * Run during initialization of the web data connector.
   */
  connector.init = function () {
    switch (tableau.phase) {
      case tableau.phaseEnum.interactivePhase:
        // Perform set up tasks that relate to when the user will be prompted to
        // enter information interactively.
        break;

      case tableau.phaseEnum.gatherDataPhase:
        // Perform set up tasks that should happen when Tableau is attempting to
        // retrieve data from your connector (the user is not prompted for any
        // information in this phase.
        break;

      case tableau.phaseEnum.authPhase:
        // Perform set up tasks that should happen when Tableau is attempting to
        // refresh OAuth authentication tokens.
        break;
    }

    tableau.initCallback();
  };

  /**
   * Run during shutdown of the web data connector.
   */
  connector.shutdown = function () {
    tableau.shutdownCallback();
  };

  /**
   * Primary method called when Tableau is asking for the column headers that
   * this web data connector provides.
   */
  connector.getColumnHeaders = function() {
    var fieldNames = [],
        fieldTypes = [],
        url = connector.getConnectionData('url'),
        metaUrl = appendQueryParam(url, 'wdc_headers', '1');

    getData(metaUrl, function getHeaders(headers, next) {
      var flatHeaders = wdcUtil.flattenHeaders(headers);

      flatHeaders.forEach(function shapeHeaders(header) {
        fieldNames.push(header.name);
        fieldTypes.push(header.type);
      });

      tableau.headersCallback(fieldNames, fieldTypes);
    });
  };

  /**
   * Primary method called when Tableau is asking for your web data connector's
   * data.
   */
  connector.getTableData = function() {
    var processedData = [],
        url = connector.getConnectionData('url');

    getData(url, function getNextData(data, next) {
      // Process our data and add to the array of results.
      data.forEach(function shapeData(item) {
        processedData.push(wdcUtil.flattenData(item));
      });

      if (next) {
        getData(next, getNextData);
      }
      else {
        tableau.dataCallback(processedData, null, false);
      }
    });
  };

  /**
   *  Get the connection data.
   *
   *  @param {string} property
   *   (optional) value to return.
   */
  connector.getConnectionData = function(property) {
    var data = tableau.connectionData ? JSON.parse(tableau.connectionData) : {};

    if (data.hasOwnProperty(property)) {
      return data[property];
    }
    else {
      return data;
    }
  };

  /**
   *  Turns a settings array into a JSON object and sends it to Tableau.
   *  @param {array} data
   */
  connector.setConnectionData = function(data) {
    tableau.connectionData = JSON.stringify(data);
  };

  /**
   * Update/Append query parameter to a given url.
   *
   * @param url
   *  The url which needs to have query params appended to.
   * @param key
   *  The name of the query parameter.
   * @param value
   *  The query param value.
   * @returns {string}
   *  The new url with the append query param(s)
   */
  function appendQueryParam(url, key, value) {
    var re = new RegExp("([?|&])" + key + "=.*?(&|#|$)", "i");

    if (url.match(re)) {
      return url.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
      var hash =  '',
          separator = url.indexOf('?') !== -1 ? "&" : "?";

      if (url.indexOf('#') !== -1 ) {
        hash = url.replace(/.*#/, '#');
        url = url.replace(/#.*/, '');
      }

      return url + separator + key + "=" + value + hash;
    }
  }

  /**
   * Gets a url to the next page of the result set.
   *
   * @param url
   *  The base url.
   * @param pager
   *  The meta data ['pager'] from our response header.
   * @returns {*}
   *  The url to our next page, or false if no next page was found.
   */
  function getNextPage(url, pager) {
    var next = false,
        current = pager ? pager.page : 0,
        total = pager ? pager.pages : 0;

    if (current < total - 1) {
      next = appendQueryParam(url, 'page', current + 1);
    }

    return next;
  }

  /**
   * Ajax call to our API
   *
   * @param {string} url
   *  The url used for our API call.
   * @param {function(data, next)} callback
   *  A callback function which takes two arguments:
   *   data: result set from the API call.
   *   next: A url to our next page (if any) or false if no next page was found.
   */
  function getData(url, callback) {
    $.ajax({
      url: url
    }).done(function(result, textStatus, jqXHR) {
      var data = [],
          pager = jqXHR.getResponseHeader('WDC-Pager') ?
            JSON.parse(jqXHR.getResponseHeader('WDC-Pager')) : null,
          next = getNextPage(url, pager);

       if (typeof result === 'object' && result.hasOwnProperty('data')) {
         callback(result.data, next);
       }
       else {
         tableau.abortWithError('Unexpected data structure.');
       }
    }).fail(function (jqXHR, textStatus, error) {
      tableau.abortWithError('Invalid API call: ' + url + '\n Please check your syntax. Error:' + error);
    });
  }

  // Registers our web data connector.
  tableau.registerConnector(connector);

  $(document).ready(function() {
    /**
     * The main click event to start fetching the data.
     */
    $('.tableau-wdc-data-source').click(function(e) {
      // Prevent the default Drupal postback.
      e.preventDefault();

      //todo If not inside Tableau, show a warning.
      /*if () {
        alert("Not connected to Tableau! Please use the web data connector from within the Tableau Software.");
        return;
      }*/

      var dataSourceId = $(this).attr('data-source-id'),
          connectionName = Drupal.settings.tableau_wdc[dataSourceId].name,
          connectionData = Drupal.settings.tableau_wdc[dataSourceId];

      connector.setConnectionData(connectionData);
      tableau.connectionName = connectionName;

      tableau.submit();
    });
  });

})(jQuery, tableau);
