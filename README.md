# Tableau Web Data Connector module
This module allows editors to easily expose data to the tableau web data connector.
Paging is supported and will automatically be taken care of.

Once you have created your endpoints, you can add the tableau-wdc block to any
page and it will automatically render a button for each data source together
with all the necessary scripts to parse and prepare the data for import

Note: This module is heavily based on views_data_source for rendering the JSON,
but adds a few settings to define the format of each column which is required
by the web data connector.

# How to use?
1. Create a view (page with a url) with the tableau-wdc style.
2. Add a couple of fields and set a label for each of them.
3. Define the column format for each field in the display settings. By default
it will set a format compatible with the field with string as the fallback format.
4. Save the view and test the output by checking the following urls:
  * view_url
    The JSON output of the content.
  * view_url?wdc-headers=1
    This url will render the meta information Tableau needs to parse the fields
    to their correct format.
5. Add the tableau-wdc block to any page.
6. Open the web data connector from Tableau and navigate to the page which has
the tableau WDC block.
7. Import the data and start creating some vizzes.
