<?php
/**
 * @file
 * Views style plugin to render nodes in the JSON data format.
 *
 * @see views_plugin_style_json.inc
 * @ingroup views_plugins
 */

/**
 * Implements hook_views_plugins().
 */
function tableau_wdc_views_plugins() {
  $path = drupal_get_path('module', 'tableau_wdc') . '/views';

  return array(
    'module' => 'tableau_wdc',
    'style' => array(
      'tableau_wdc' => array(
        'title' => t('Tableau WDC compatible JSON data document'),
        'path' => $path . '/plugins',
        'help' => t('Displays data in JSON format with Tableau WDC headers.'),
        'handler' => 'views_plugin_style_tableau_wdc',
        'theme' => 'views_tableau_wdc',
        'theme file' => 'views_tableau_wdc.theme.inc',
        'theme path' => $path . '/theme',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'help_topic' => 'style-tableau-wdc',
        'even empty' => TRUE,
      ),
    ),
  );
}

/**
 * Implements hook_views_query_alter().
 */
function tableau_wdc_views_query_alter(&$view, &$query) {
  $show_headers = isset($_GET['wdc_headers']);

  if ($view->style_plugin->plugin_name === 'tableau_wdc' && $show_headers) {
    // Stop view execution if we are showing header information.
    $view->executed = true;
    $view->built = true;
  }
}
