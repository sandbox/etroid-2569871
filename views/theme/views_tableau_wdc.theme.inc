<?php

/**
 * @file
 * Views theme to render view fields as WDC compatible JSON.
 *
 * - $view: The view in use.
 * - $rows: Array of row objects as rendered by _views_json_render_fields
 * - $attachment: Not used currently
 * - $options: The options for the style passed in from the UI.
 *
 */
function template_preprocess_views_tableau_wdc(&$vars) {
  $show_headers = isset($_GET['wdc_headers']) ? true : false;
  global $pager_total, $pager_page_array, $pager_total_items, $pager_limits;

  module_load_include('inc', 'views_json', '/views/theme/views_views_json_style.theme');
  template_preprocess_views_views_json_style_simple($vars);

  $vars['pager'] = array(
    'pages' => intval($pager_total[0]),
    'page' => intval($pager_page_array[0]),
    'count' => intval($pager_total_items[0]),
    'limit' => intval($pager_limits[0])
  );

  if ($show_headers) {
    $headers = array();
    $options = $vars['options'];
    $wdc_info = $options['wdc_info'];

    foreach ($wdc_info as $field => $info) {
      $label = $info['label'];
      $format = $info['format'];

      $headers[$label] = $format;
    }

    $vars['rows']['data'] = $headers;
  }
}

/**
 * Theme the form for the table style plugin
 */
function theme_views_tableau_wdc_table($vars) {
  $form = $vars['form'];
  $header = array(
    t('Field'),
    t('WDC Format'),
  );
  $rows = array();

  foreach (element_children($form['wdc_info']) as $id) {
    $row = array();
    $row[] = array(
      'data' => drupal_render($form['wdc_info'][$id]['field']),
    );
    $row[] = array(
      'data' => drupal_render($form['wdc_info'][$id]['format']),
    );
    $rows[] = $row;
  }

  $output = theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}
