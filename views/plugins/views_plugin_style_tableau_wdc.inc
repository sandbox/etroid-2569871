<?php
/**
 * @file
 * Implements views_plugin_style_json for tableau_wdc
 */

/**
 * Implements views_plugin_style
 */
class views_plugin_style_tableau_wdc extends views_plugin_style_json {

  /**
   * Implements views_plugin_style::option_definition
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['root_object']['default'] = 'data';
    $options['top_child_object']['default'] = '';
    $options['pretty_print']['default'] = TRUE;
    $options['wdc_info'] = array('default' => array());

    return $options;
  }

  /**
   * Provide a form for setting options.
   */
  function options_form(&$form, &$form_state) {
    $form['#theme'] = 'views_tableau_wdc_table';
    $field_names = $this->display->handler->get_field_labels();
    $fields = $this->display->handler->get_option('fields');

    foreach($fields as $field => $info) {
      $table = $info['table'];

      $form['wdc_info'][$field]['field'] = array(
        '#id' => 'wdc-' . $field . '-field',
        '#type' => 'item',
        '#markup' => isset($info['label']) ? $info['label'] : $field_names[$field],
      );

      $form['wdc_info'][$field]['label'] = array(
        '#id' => 'wdc-' . $field . '-label',
        '#type' => 'value',
        '#value' => isset($info['label']) ? $info['label'] : $field_names[$field],
      );

      $form['wdc_info'][$field]['format'] = array(
        '#id' => 'wdc-' . $field . '-format',
        '#attributes' => array(
          'style' => array('width: 100%'),
        ),
        '#type' => 'select',
        '#options' => array(
          'string' => t('String'),
          'int' => t('Integer'),
          'float' => t('Float'),
          'bool' => t('Boolean'),
          'date' => t('Date'),
          'datetime' => t('Date/Time'),
        ),
        '#multiple' => FALSE,
        '#default_value' => isset($this->options['wdc_info'][$field]['format'])
          ? $this->options['wdc_info'][$field]['format']
          : tableau_wdc_get_wdc_field_type($field, $table),
      );
    }
  }

  /**
   * Implementation of view_style_plugin::theme_functions().
   * Returns an array of theme functions to use for the current style plugin
   * @return array
   */
  function theme_functions() {
    $hook = 'views_tableau_wdc';
    return views_theme_functions($hook, $this->view, $this->display);
  }

  /**
   * Implementation of view_style_plugin::render()
   */
  function render() {
    $view = $this->view;
    $options = $this->options;

    $rows = array();
    foreach ($view->result as $count => $row) {
      $view->row_index = $count;
      $rows[] = _views_json_render_fields($view, $row);
    }
    unset($view->row_index);

    return theme($this->theme_functions(), array(
      'view' => $view,
      'options' => $options,
      'rows' => $rows
    ));
  }

}
